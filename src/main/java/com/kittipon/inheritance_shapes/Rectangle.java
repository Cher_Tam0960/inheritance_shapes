/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.inheritance_shapes;

/**
 *
 * @author kitti
 */
public class Rectangle extends Shape{

    private double w;
    private double l;

    public Rectangle(double w, double l) {
        if (w == l) {
            System.out.println("Square has created.");
        } else {
            System.out.println("Rectangle has created.");
        }

        this.w = w;
        this.l = l;
    }

    @Override
    public double calArea() {
        double result = w * l;
        return result;
    }

    public void print() {
        System.out.println("Total area is " +calArea());
    }
}
