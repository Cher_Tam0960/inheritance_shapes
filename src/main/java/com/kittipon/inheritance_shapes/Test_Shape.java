/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.inheritance_shapes;

/**
 *
 * @author kitti
 */
public class Test_Shape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();
        NextLine();
        Circle circle2 = new Circle(4);
        circle2.print();
        NextLine();
        
        Triangle triangle1 = new Triangle(3, 4);
        triangle1.print();
        NextLine();
        
        Rectangle rectangle1 = new Rectangle(3,4);
        rectangle1.print();
        NextLine();
        Rectangle rectangle2 = new Rectangle(2,2);
        rectangle2.print();
        NextLine();
    }

    private static void NextLine() {
        System.out.println();
    }
    
}
